package demo.com.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    // Creating the Object
    EditText T1,T2,T3;
    Button btn007,btn006,btn005,btn004;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Binding With the id
        T1= (EditText) findViewById(R.id.no1);
        T2= (EditText) findViewById(R.id.no2);
        T3= (EditText) findViewById(R.id.Ans);
        btn007= (Button) findViewById(R.id.btn1);
        btn006= (Button) findViewById(R.id.btn2);
        btn005 = (Button) findViewById(R.id.btn3);
        btn004 = (Button) findViewById(R.id.btn4);


        btn007.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int t1,t2,ans;

                // Get the Value From the user
                t1 = Integer.parseInt(T1.getText().toString());
                t2 = Integer.parseInt(T2.getText().toString());

                ans = t1 + t2;

                // set the value
                T3.setText(ans + " ");



            }
        });


        btn006.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int t1,t2,ans;
                t1 = Integer.parseInt(T1.getText().toString());
                t2 = Integer.parseInt(T2.getText().toString());

                ans = t1 - t2;


                T3.setText(ans + " ");




            }
        });


        btn005.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int t1,t2,ans;
                t1 = Integer.parseInt(T1.getText().toString());
                t2 = Integer.parseInt(T2.getText().toString());

                ans = t1 * t2;


                T3.setText(ans + " ");



            }
        });



        btn004.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int t1,t2,ans;
                t1 = Integer.parseInt(T1.getText().toString());
                t2 = Integer.parseInt(T2.getText().toString());

                ans = t1 / t2;


                T3.setText(ans + " ");



            }
        });

    }
}
