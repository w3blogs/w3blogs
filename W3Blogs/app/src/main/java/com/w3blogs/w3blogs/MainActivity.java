package com.w3blogs.w3blogs;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Fragment f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Fragment f=new HomeFragment();
        FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frm, f);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onKeyDown(final int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(MainActivity.this);
            alertbox.setTitle("Are You Sure");
            alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    // finish used for destroyed activity
                    finish();

                }
            });

            alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {


                }
            });


            alertbox.show();
        }


        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.rateus) {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + this.getPackageName()));
            startActivity(rateIntent);
            return true;
        }
        if (id == R.id.feedback) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("message/rfc822");
            List<ResolveInfo> queryIntentActivities = getPackageManager().queryIntentActivities(intent, 0);
            if (queryIntentActivities.isEmpty()) {
            } else {
                boolean jd = false;
                for (ResolveInfo resolveInfo : queryIntentActivities) {
                    boolean email;
                    if (resolveInfo.activityInfo.packageName.toLowerCase().contains("com.google.android.gm") || resolveInfo.activityInfo.name.toLowerCase().contains("com.google.android.gm")) {
                        intent.setType("message/Email");
                        intent.putExtra("android.intent.extra.EMAIL", new String[]{"w3blogs.developer@gmail.com"});
                        intent.putExtra("android.intent.extra.SUBJECT", "Query at The Android Hub...!!");
                        intent.setPackage(resolveInfo.activityInfo.packageName);
                        startActivity(Intent.createChooser(intent, "Send mail..."));
                        email = true;
                    } else {
                        email = jd;
                    }
                    jd = email;
                }
            }


            return true;
        }

            return super.onOptionsItemSelected(item);
        }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        if (id == R.id.action_home) {

            f=new HomeFragment();
            FragmentManager fm = getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frm, f);
            fragmentTransaction.commit();

        } else if (id == R.id.action_bookmarks) {
            f=new BookmarkFragment();
            FragmentManager fm = getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frm, f);
            fragmentTransaction.commit();

        } else if (id == R.id.action_rateus) {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + this.getPackageName()));
            startActivity(rateIntent);

        } else if (id == R.id.action_feedback) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("message/rfc822");
            List<ResolveInfo> queryIntentActivities = getPackageManager().queryIntentActivities(intent, 0);
            if (queryIntentActivities.isEmpty()) {
            } else {
                boolean jd = false;
                for (ResolveInfo resolveInfo : queryIntentActivities) {
                    boolean email;
                    if (resolveInfo.activityInfo.packageName.toLowerCase().contains("com.google.android.gm") || resolveInfo.activityInfo.name.toLowerCase().contains("com.google.android.gm")) {
                        intent.setType("message/Email");
                        intent.putExtra("android.intent.extra.EMAIL", new String[]{"w3blogs.developer@gmail.com"});
                        intent.putExtra("android.intent.extra.SUBJECT", "Query at The Android Hub...!!");
                        intent.setPackage(resolveInfo.activityInfo.packageName);
                        startActivity(Intent.createChooser(intent, "Send mail..."));
                        email = true;
                    } else {
                        email = jd;
                    }
                    jd = email;
                }
            }

        } else if (id == R.id.action_share) {
            f=new ShareFragment();
            FragmentManager fm = getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frm, f);
            fragmentTransaction.commit();

        } else if (id == R.id.nightmode) {

        }




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
