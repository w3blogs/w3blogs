package com.w3blogs.w3blogs.Adapter;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.w3blogs.w3blogs.BookmarkFragment;
import com.w3blogs.w3blogs.HomeFragment;
import com.w3blogs.w3blogs.MainActivity;
import com.w3blogs.w3blogs.Model.DataModel;
import com.w3blogs.w3blogs.R;
import com.w3blogs.w3blogs.SharedPreference;


import java.util.ArrayList;
import java.util.Locale;

public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener {

    private ArrayList<DataModel> dataSet;
    private ArrayList<DataModel> arraylist;
    Context mContext;
    LayoutInflater inflater;
    SharedPreference sharedPref;
    BookmarkFragment book;

    // View lookup cache
    private static class ViewHolder {
        ImageView imgdemo;
        TextView txtName;

        ImageButton btn;
    }

    public CustomAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.design_file, data);
        this.dataSet = data;
        this.mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<DataModel>();
        this.arraylist.addAll(data);

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        dataSet.clear();
        if (charText.length() == 0) {
            dataSet.addAll(arraylist);
        } else {
            for (DataModel wp : arraylist) {
                if (wp.getNameofprogram().toLowerCase(Locale.getDefault()).contains(charText)) {
                    dataSet.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


    @Override
    public void onClick(View v) {

        //  int position=(Integer) v.getTag();
        // Object object= getItem(position);
        //  DataModel dataModel=(DataModel)object;


    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataModel dataModel = getItem(position);
        final ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.design_file, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.imgdemo = (ImageView) convertView.findViewById(R.id.imgdemo);
            viewHolder.btn = convertView.findViewById(R.id.btn);
            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
            viewHolder.txtName.setText(dataSet.get(position).getNameofprogram());
            viewHolder.imgdemo.setImageResource(dataSet.get(position).getImage());
            return convertView;


        }

        viewHolder.txtName.setText(dataModel.getNameofprogram());
        viewHolder.imgdemo.setImageResource(dataModel.getImage());
        viewHolder.imgdemo.setTag(dataModel.getImage());
        viewHolder.imgdemo.setOnClickListener(this);
        final View finalConvertView = convertView;

        sharedPref = new SharedPreference();

        viewHolder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //For bookmark
                Snackbar sb = Snackbar.make(finalConvertView, "Bookmark Added", 2000);
                sb.show();
                DataModel p = new DataModel();
                p.setNameofprogram("demo");

                Toast.makeText(getContext(), p.getNameofprogram(), Toast.LENGTH_SHORT).show();
                sharedPref.addFavorite(getContext(), p);


            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "kk", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }
}