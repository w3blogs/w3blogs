package com.w3blogs.w3blogs;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.w3blogs.w3blogs.Model.DataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 25/12/17.
 */

public class BookmarkFragment extends Fragment {
    ListView lst;
    List<DataModel> allPost;//=new ArrayList<>();

    SharedPreference sharedPref=new SharedPreference();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bookmarkfragment,container,false);

    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("BookMark");
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        allPost=sharedPref.getFavorites(getContext());

        ArrayList<String> list=new ArrayList<String>();
        lst=getActivity().findViewById(R.id.lst);

        for(int i=0;i< allPost.size();i++){
            list.add(allPost.get(i).getNameofprogram());
        }

        ArrayAdapter<String> aa=new ArrayAdapter<String>(getContext(),R.layout.support_simple_spinner_dropdown_item,list);
        lst.setAdapter(aa);
    }
}
