package com.w3blogs.w3blogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.w3blogs.w3blogs.Model.DataModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SharedPreference {

    public static final String PREFS_NAME = "POST_APP";
    public static final String FAVORITES = "Post_Favorite";

    public SharedPreference() {
        super();
    }

    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<DataModel> favorites) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public void addFavorite(Context context, DataModel product) {
        List<DataModel> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<DataModel>();
        favorites.add(product);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, DataModel product) {
        ArrayList<DataModel> favorites = getFavorites(context);
        if (favorites != null) {

            int i = 0;
            for (DataModel m : favorites) {
                if (m.getNameofprogram().equals(product.getNameofprogram())) {
                    favorites.remove(i);
                    break;
                }
                i++;
            }

            saveFavorites(context, favorites);
        }
    }

    public ArrayList<DataModel> getFavorites(Context context) {
        SharedPreferences settings;
        List<DataModel> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            DataModel[] favoriteItems = gson.fromJson(jsonFavorites,
                    DataModel[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<DataModel>(favorites);
        } else
            return null;

        return (ArrayList<DataModel>) favorites;
    }
}
