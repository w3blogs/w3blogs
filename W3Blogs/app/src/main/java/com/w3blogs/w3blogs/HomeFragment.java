package com.w3blogs.w3blogs;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.w3blogs.w3blogs.Adapter.CustomAdapter;
import com.w3blogs.w3blogs.Model.DataModel;

import java.util.ArrayList;

/**
 * Created by root on 13/12/17.
 */

public class HomeFragment extends Fragment implements SearchView.OnQueryTextListener {
    ArrayList<DataModel> data=new ArrayList<>();
    ListView lst;
    Fragment f;
    SearchView editsearch;
    private static CustomAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment,container,false);

    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lst = (ListView) getView().findViewById(R.id.lst);

        data.add(new DataModel(R.drawable.textview, "Textview"));
        data.add(new DataModel(R.drawable.edittext, "EditText"));
        data.add(new DataModel(R.drawable.button, "Button"));
        data.add(new DataModel(R.drawable.radiobutton, "RadioButton"));
        data.add(new DataModel(R.drawable.chechbox, "ChechBox"));
        data.add(new DataModel(R.drawable.ratingbar, "RatingBar"));
        data.add(new DataModel(R.drawable.progress, "Progressbar"));
        data.add(new DataModel(R.drawable.seekbar, "SeekBar"));
        data.add(new DataModel(R.drawable.textview, "Switch"));
        data.add(new DataModel(R.drawable.w3bloglogo, "ToggleButton"));
        data.add(new DataModel(R.drawable.w3bloglogo, "Spinner"));


        adapter = new CustomAdapter(data,getContext());
        lst.setAdapter(adapter);

        editsearch = (SearchView) getActivity().findViewById(R.id.search);
        editsearch.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adapter.filter(text);
        return false;
    }
}
