package com.w3blogs.intentdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText ed;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ed= findViewById(R.id.de1);
        btn=findViewById(R.id.btn);
        String text = ed.getText().toString();


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View text) {

                if (ed.getText().length()==0) {
                    Toast.makeText(MainActivity.this, "You Have No Input Any Value", Toast.LENGTH_SHORT).show();
                }

                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    intent.putExtra("name", ed.getText().toString());
                    startActivity(intent);

                }
        });
    }
}
