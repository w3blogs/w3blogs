package demo.com.checkbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Create the object of the Button and Checkbox
    Button b;
    CheckBox c;
    CheckBox c1;
    CheckBox c2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Bindding With id
            b= findViewById( R.id.btn1);
            c= (CheckBox) findViewById(R.id.cb);
            c1 = (CheckBox) findViewById(R.id.cb1);
            c2 = (CheckBox) findViewById(R.id.cb2);

        //Button OnclickListener is called
        b.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 // Checked Which CheckBox is Checked And print their nanme in toast
                 if(c.isChecked()){
                     Toast.makeText(MainActivity.this, "Red Is Checked....!", Toast.LENGTH_SHORT).show();
                 }
                 if (c1.isChecked()
                         ) {
                     Toast.makeText(MainActivity.this, "Black Is Checked....!", Toast.LENGTH_SHORT).show();
                 }
                 if (c2.isChecked()
                         ) {

                     Toast.makeText(MainActivity.this, "Blue Is Checked....!", Toast.LENGTH_SHORT).show();
                 }
             }



         });
    }
}
