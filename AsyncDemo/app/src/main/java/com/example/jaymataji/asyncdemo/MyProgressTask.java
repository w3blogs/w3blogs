package com.example.jaymataji.asyncdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Jay Mataji on 22-03-2018.
 */

public class MyProgressTask extends AsyncTask<Void,Integer, Integer> {

    Context ctx;
    ProgressDialog pd;

    public MyProgressTask(MainActivity activity) {
        ctx=activity;
    }

    @Override
    protected void onPreExecute() {
        pd = new ProgressDialog(ctx);
        pd.setTitle("Downloading");
        pd.setMessage("Please Wait...");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMax(100);
        pd.setButton(ProgressDialog.BUTTON_POSITIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancel(true);
            }
        });

        pd.show();
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        try {

            for (int i = 1; i <= 100; i++) {
                Thread.sleep(100);
                publishProgress(i);
                Log.i("Thread Message",i+"");
            }

            return 1;
        } catch (Exception e) {
            Log.i("Exception", e.getMessage());
            return null;
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        int value = values[0];
        pd.setProgress(value);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        pd.dismiss();
        Toast.makeText(ctx,integer+"",Toast.LENGTH_LONG).show();
    }
}
