package com.w3blogs.customtoastdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnCustomToast = (Button)findViewById(R.id.btn_showToast);
        btnCustomToast.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                LayoutInflater inflater = getLayoutInflater();
                View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup)findViewById(R.id.toastcustom));
                ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("W3 blogs...!!");

                Toast custom = new Toast(getBaseContext());
                custom.setView(layouttoast);
                custom.setDuration(Toast.LENGTH_LONG);
                custom.setGravity(Gravity.TOP,0,0);
                custom.show();

                Toast custom1 = new Toast(getBaseContext());
                custom1.setView(layouttoast);
                custom1.setDuration(Toast.LENGTH_LONG);
                custom1.setGravity(Gravity.CENTER,0,0);
                custom1.show();

                Toast custom2 = new Toast(getBaseContext());
                custom2.setView(layouttoast);
                custom2.setDuration(Toast.LENGTH_LONG);
                custom2.setGravity(Gravity.BOTTOM,0,0);
                custom2.show();

                Toast custom3 = new Toast(getBaseContext());
                custom3.setView(layouttoast);
                custom3.setDuration(Toast.LENGTH_LONG);
                custom3.setGravity(Gravity.LEFT,0,0);
                custom3.show();

                Toast custom4 = new Toast(getBaseContext());
                custom4.setView(layouttoast);
                custom4.setDuration(Toast.LENGTH_LONG);
                custom4.setGravity(Gravity.RIGHT,0,0);
                custom4.show();

                Toast custom5 = new Toast(getBaseContext());
                custom5.setView(layouttoast);
                custom5.setDuration(Toast.LENGTH_LONG);
                custom5.setGravity(Gravity.TOP|Gravity.LEFT,0,0);
                custom5.show();

                Toast custom6 = new Toast(getBaseContext());
                custom6.setView(layouttoast);
                custom6.setDuration(Toast.LENGTH_LONG);
                custom6.setGravity(Gravity.BOTTOM|Gravity.RIGHT,0,0);
                custom6.show();
            }
        });

    }
}
