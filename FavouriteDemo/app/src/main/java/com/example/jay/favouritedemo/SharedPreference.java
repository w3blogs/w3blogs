package com.example.jay.favouritedemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SharedPreference {

    public static final String PREFS_NAME = "POST_APP";
    public static final String FAVORITES = "Post_Favorite";

    public SharedPreference() {
        super();
    }

    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<post> favorites) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public void addFavorite(Context context, post product) {
        List<post> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<post>();
        favorites.add(product);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, post product) {
        ArrayList<post> favorites = getFavorites(context);
        if (favorites != null) {

            int i = 0;
            for (post m : favorites) {
                if (m.getName().equals(product.getName())) {
                    favorites.remove(i);
                    break;
                }
                i++;
            }

            saveFavorites(context, favorites);
        }
    }

    public ArrayList<post> getFavorites(Context context) {
        SharedPreferences settings;
        List<post> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            post[] favoriteItems = gson.fromJson(jsonFavorites,
                    post[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<post>(favorites);
        } else
            return null;

        return (ArrayList<post>) favorites;
    }
}
