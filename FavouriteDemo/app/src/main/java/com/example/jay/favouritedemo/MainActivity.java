package com.example.jay.favouritedemo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.internal.bind.ArrayTypeAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button b1,b2;
    ListView lst;
    List<post> allPost;

    SharedPreference sharedPref;
    SharedPreference sharedPref1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1=findViewById(R.id.btnAdd);
        b2=findViewById(R.id.btnList);
        lst=findViewById(R.id.lst);


        sharedPref=new SharedPreference();



        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                post p=new post();
                p.setName("chetan");

                Toast.makeText(MainActivity.this, p.getName(), Toast.LENGTH_SHORT).show();
                sharedPref.addFavorite(MainActivity.this,p);

            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                allPost=sharedPref.getFavorites(MainActivity.this);

                ArrayList<String> list=new ArrayList<String>();

                for(int i=0;i< allPost.size();i++){
                    list.add(allPost.get(i).getName());
                }

                ArrayAdapter<String> aa=new ArrayAdapter<String>(MainActivity.this,R.layout.activity_cell,list);
                lst.setAdapter(aa);
            }
        });
    }
}
