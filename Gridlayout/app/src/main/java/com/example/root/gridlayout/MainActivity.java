package com.example.root.gridlayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {

    // Create object
    GridView g;
    // create the array which name is number
    static  final String[] number = new String[]{
      "A","B","C","D","E",
      "F","G","H","I","J",
      "K","L","M","N","O",
      "P","Q","R","S","T",
      "U","V","W","X","Y","Z"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // bind with id
        g = findViewById(R.id.g1);

        //create the adepter and set data in it
        ArrayAdapter<String>arr = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,number);

        //set the adepter in the gridview
        g.setAdapter(arr);



    }
}
