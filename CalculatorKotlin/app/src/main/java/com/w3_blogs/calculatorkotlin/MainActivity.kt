package com.w3_blogs.calculatorkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val edt1 = findViewById(R.id.ed1) as EditText
        val edt2 = findViewById(R.id.ed2) as EditText
        val result = findViewById(R.id.tv_result) as TextView
        val btnadd = findViewById(R.id.btn_add) as Button
        val btnsub = findViewById(R.id.btn_sub) as Button
        val btnmul = findViewById(R.id.btn_mul) as Button
        val btndiv = findViewById(R.id.btn_div) as Button

        btnadd.setOnClickListener {

            val a: String = edt1.text.toString()
            val b: String = edt2.text.toString()

            if (edt1.text.isEmpty() && edt2.text.isEmpty()) {
                Toast.makeText(applicationContext, "Enter the Value", Toast.LENGTH_SHORT).show()
            } else {
                val c = a.toInt()
                val d = b.toInt()
                val e = c + d
                result.setText(e.toString())
            }
        }

        btnsub.setOnClickListener {
            if (edt1.text.isEmpty() || edt2.text.isEmpty()) {
                Toast.makeText(applicationContext, "Enter the Value", Toast.LENGTH_SHORT).show()
            } else {
                val a: Int = edt1.text.toString().toInt()
                val b: Int = edt2.text.toString().toInt()
                val c: Int = a - b
                result.setText(c.toString())
            }

            btnmul.setOnClickListener { v ->
                result.setText(kali(edt1.text.toString().toInt(), edt2.text.toString().toInt()).toString())
            }

            btndiv.setOnClickListener { v ->
                result.text = (getEdt1() / getEdt2()).toString()
            }
        }
    }

    fun kali(a: Int, b: Int): Int {
        return a * b
    }
    fun getEdt1(): Int {
        val edt1 = findViewById(R.id.edt_satu) as EditText
        val a: String = edt1.text.toString()
        return a.toInt()
    }

    fun getEdt2(): Int {
        val edt2 = findViewById(R.id.edt_dua) as EditText
        val b: String = edt2.text.toString()
        return b.toInt()
    }
}


