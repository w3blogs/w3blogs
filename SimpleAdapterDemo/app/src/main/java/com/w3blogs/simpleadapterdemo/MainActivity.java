package com.w3blogs.simpleadapterdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    String[] animalName={"panda","Leopard","Lion","Bear","Tiger","Monkey","Deer","Dog","Cat","Elephant"};//animal names array
    int[] animalImages={R.drawable.panda,R.drawable.leopard, R.drawable.lion,R.drawable.bear,R.drawable.tiger,R.drawable.monkey,R.drawable.deer,R.drawable.dog,R.drawable.cat,R.drawable.elephent};//animal images array
    ListView simpleListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        simpleListView=(ListView)findViewById(R.id.simpleListView);

        ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for (int i=0;i<animalName.length;i++)
        {
            //create a hashmap to store the data in key value pair
            HashMap<String,String> hashMap=new HashMap<>();
            hashMap.put("name",animalName[i]);
            hashMap.put("image",animalImages[i]+"");
            //add the hashmap into arrayList
            arrayList.add(hashMap);
        }
        //string array
        String[] from={"name","image"};
        //int array of views id's
        int[] to={R.id.textView,R.id.imageView};
        //Create object and set the parameters for simpleAdapter
        SimpleAdapter simpleAdapter=new SimpleAdapter(this,arrayList,R.layout.activity_design,from,to);
        //sets the adapter for listView
        simpleListView.setAdapter(simpleAdapter);

        //perform listView item click event
        simpleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //show the selected image in toast according to position
                Toast.makeText(getApplicationContext(),animalName[i], Toast.LENGTH_LONG).show();
            }
        });

    }
}
