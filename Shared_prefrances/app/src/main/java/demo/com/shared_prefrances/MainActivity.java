package demo.com.shared_prefrances;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText e1, e2;
    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.no);
        b = (Button) findViewById(R.id.btn);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();

        String s = pref.getString("email", null);
        String n = pref.getString("num", null);

        if (s == null && n == null) {

            ed.putString("email", e1.getText().toString());
            ed.putString("num", e2.getText().toString());
            ed.commit();

        } else {
            Intent ob = new Intent(MainActivity.this, Main2Activity.class);
            startActivity(ob);
            Toast.makeText(this, "Already Registerd", Toast.LENGTH_SHORT).show();

        }

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if (e1.getText().toString().trim().length()<=0) {
                   e1.setError("Enter Email");

               }else if (e2.getText().toString().trim().length()<=0){
                    e2.setError("number");

                } else {
                    Intent ob = new Intent(MainActivity.this, Main2Activity.class);
                    startActivity(ob);
                    Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
