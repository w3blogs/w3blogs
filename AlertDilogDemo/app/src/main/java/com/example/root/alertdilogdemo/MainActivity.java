package com.example.root.alertdilogdemo;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    // object of button
    Button btnClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnClick=(Button)findViewById(R.id.btn);

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertShow();
            }
        });
    }

    @Override
    public void onBackPressed() {

        alertShow();
    }



    public void alertShow(){


        //Setting message manually and performing action on button click
        AlertDialog.Builder Bulider=new AlertDialog.Builder(MainActivity.this);
        Bulider.setMessage("Do you Want to Close this App...")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       //Action for OK Button
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        //Creating dialog box
        AlertDialog alert=Bulider.create();
        //Setting the title manually
        alert.setTitle("Alert ....");
        alert.show();

    }

    }
