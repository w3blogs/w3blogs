package demo.com.framelayout;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Fragment frm;
    Button bl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bl = findViewById(R.id.btn);

        bl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frm= new fragment();
                FragmentManager fm= getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fm,frm);
                ft.commit();

            }
        });
    }
}
