package demo.com.studentmarksheet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Vivek on 11/7/2017.
 */

public class DBManager {

    private DBHelper dbHelper;
    private Context context;
    private SQLiteDatabase database;


    public DBManager(Context c) {
        context = c;
    }


    public DBManager open() throws SQLException {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String Name, int Sub1, int Sub2, int Sub3, int Total, String Grade, float Percentage) {

        ContentValues contentValue = new ContentValues();

        contentValue.put(DBHelper.STU_NAME, Name);
        contentValue.put(DBHelper.STU_SUB1, Sub1);
        contentValue.put(DBHelper.STU_SUB2, Sub2);
        contentValue.put(DBHelper.STU_SUB3, Sub3);
        contentValue.put(DBHelper.STU_TOTAL, Total);
        contentValue.put(DBHelper.STU_GRADE, Grade);
        contentValue.put(DBHelper.STU_PER, Percentage);

        database.insert(DBHelper.TABLE_NAME, null, contentValue);
    }


    public Cursor fetch() {
        String[] columns = new String[]{DBHelper._ID, DBHelper.STU_NAME, DBHelper.STU_SUB1, DBHelper.STU_SUB2, DBHelper.STU_SUB3, DBHelper.STU_TOTAL, DBHelper.STU_GRADE, DBHelper.STU_PER};

        Cursor cursor = database.query(DBHelper.TABLE_NAME, columns, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int update(long _Id, String Name, int Sub1, int Sub2, int Sub3, int Total, String Grade, float Percentage) {
        ContentValues contentValues = new ContentValues();


        contentValues.put(DBHelper.STU_NAME, Name);
        contentValues.put(DBHelper.STU_SUB1, Sub1);
        contentValues.put(DBHelper.STU_SUB2, Sub2);
        contentValues.put(DBHelper.STU_SUB3, Sub3);
        contentValues.put(DBHelper.STU_TOTAL, Total);
        contentValues.put(DBHelper.STU_GRADE, Grade);
        contentValues.put(DBHelper.STU_PER, Percentage);

        int i = database.update(DBHelper.TABLE_NAME, contentValues, DBHelper._ID + " = " + _Id, null);

        return i;
    }

    public void delete(long _Id) {
        database.delete(DBHelper.TABLE_NAME, DBHelper._ID + "=" + _Id, null);
    }


}
