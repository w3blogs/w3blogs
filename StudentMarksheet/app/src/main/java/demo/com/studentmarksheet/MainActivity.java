package demo.com.studentmarksheet;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText id1, n1, s1, s2, s3, t1, g1, p1;
    Button c1, i1, u1, d1, f1;
    DBManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        id1 = (EditText) findViewById(R.id.id);
        n1 = (EditText) findViewById(R.id.name);
        s1 = (EditText) findViewById(R.id.sub1);
        s2 = (EditText) findViewById(R.id.sub2);
        s3 = (EditText) findViewById(R.id.sub3);
        t1 = (EditText) findViewById(R.id.total);
        g1 = (EditText) findViewById(R.id.grade);
        p1 = (EditText) findViewById(R.id.per);

        c1 = (Button) findViewById(R.id.cal);
        i1 = (Button) findViewById(R.id.insert);
        u1 = (Button) findViewById(R.id.update);
        d1 = (Button) findViewById(R.id.delete);
        f1 = (Button) findViewById(R.id.fatch);

        dm = new DBManager(MainActivity.this);

        c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int subject1, subject2, subject3,total;
                float  percentage;


                subject1 = Integer.parseInt(s1.getText().toString());
                subject2 = Integer.parseInt(s2.getText().toString());
                subject3 = Integer.parseInt(s3.getText().toString());

                total = subject1 + subject2 + subject3;
                t1.setText(total+"");

                percentage = total * 100 / 300;
                p1.setText(percentage+"");


                if (percentage < 50)
                    g1.setText("F");
                else if (percentage < 70)
                    g1.setText("C");
                else if (percentage < 80)
                    g1.setText("B");
                else if (percentage < 90)
                    g1.setText("A");
                else
                    g1.setText("A+");


            }
        });

        i1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dm.open();

                String n = n1.getText().toString();
                int subject1 = Integer.parseInt(s1.getText().toString());
                int subject2 = Integer.parseInt(s2.getText().toString());
                int subject3 = Integer.parseInt(s3.getText().toString());
                int  total1 =  Integer.parseInt(t1.getText().toString());
                String grade = g1.getText().toString();
                float percentage = Float.parseFloat(p1.getText().toString());

                dm.insert(n, subject1, subject2, subject3, total1, grade, percentage);


                dm.close();
                Toast.makeText(MainActivity.this, "Insert Successfully", Toast.LENGTH_SHORT).show();

            }
        });


        u1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dm.open();

                int  id=Integer.parseInt(id1.getText().toString());
                String n = n1.getText().toString();
                int subject11 = Integer.parseInt(s1.getText().toString());
                int subject22 = Integer.parseInt(s2.getText().toString());
                int subject33 = Integer.parseInt(s3.getText().toString());
                int  total1 =  Integer.parseInt(t1.getText().toString());
                String grade = g1.getText().toString();
                float percentage = Float.parseFloat(p1.getText().toString());

                dm.update(id,n, subject11, subject22, subject33, total1, grade, percentage);

                dm.close();
                Toast.makeText(MainActivity.this, "Updated", Toast.LENGTH_SHORT).show();

            }
        });

        d1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dm.open();

                int  id=Integer.parseInt(id1.getText().toString());
                dm.delete(id);

                dm.close();

                Toast.makeText(MainActivity.this, "Record Deleted", Toast.LENGTH_SHORT).show();
                dm.close();
            }
        });

        f1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dm.open();

                Cursor c=dm.fetch();

                c.moveToNext();


                id1.setText(c.getString(0));
                n1.setText(c.getString(1));
                s1.setText(c.getString(2));
                s2.setText(c.getString(3));
                s3.setText(c.getString(4));
                t1.setText(c.getString(5));
                g1.setText(c.getString(6));
                p1.setText(c.getString(7));
                dm.close();

                Toast.makeText(MainActivity.this, "Record Fetched", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
