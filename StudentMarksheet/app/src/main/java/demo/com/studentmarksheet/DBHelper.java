package demo.com.studentmarksheet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Vivek on 11/7/2017.
 */

public class DBHelper extends SQLiteOpenHelper {


    // Database Information
    static final String DB_NAME = "STUDEBT.DB";

    // Table Name
    public static final String TABLE_NAME = "RESULT";

    // Table columns
    public static final String _ID = "_Id";
    public static final String STU_NAME = "Name";
    public static final String STU_SUB1 = "Sub1";
    public static final String STU_SUB2="Sub2";
    public static final String STU_SUB3="Sub3";
    public static final String STU_TOTAL="Total";
    public static final String STU_GRADE="Grade";
    public static final String STU_PER="Percentage";


    // database version
    static final int DB_VERSION = 1;


    // Creating table query
    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + STU_NAME + " TEXT NOT NULL, " + STU_SUB1 + " INTEGER," + STU_SUB2 + " INTEGER," +STU_SUB3 +" INTEGER," + STU_TOTAL +" INTEGER,"+ STU_GRADE +" TEXT," +STU_PER + " FLOAT );";


    public DBHelper(Context ct) {
        super(ct, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);

    }
}
