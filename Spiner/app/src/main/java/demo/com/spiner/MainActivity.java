package demo.com.spiner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Creating The Object
    Spinner s1;

    // Object Of the Array list
    ArrayList<String> lst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Bind With id
        s1 = (Spinner) findViewById(R.id.spn1);

        // Add Value in the List
        lst = new ArrayList<String>();
        lst.add("w3blogs");
        lst.add("Android");


        // Call the Adepter and set the value in it.
        ArrayAdapter<String> arr = new ArrayAdapter<String>(getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,lst);

        //Set The Adepter tp the spinner
        s1.setAdapter(arr);


        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
             ;
                Toast.makeText(MainActivity.this, lst.get(i), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
}
