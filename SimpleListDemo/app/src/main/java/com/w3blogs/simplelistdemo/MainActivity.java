package com.w3blogs.simplelistdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView lstview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Array of strings...
        final String Country_Name[]={"United States","Brazil","China","India","Russia","Indonesia","Pakistan","Mexico","Bangladesh","Nigeria","Japan","Iran","Taiwan","Korea","Sri Lanka","Iraq"};
        ArrayAdapter<String> array=new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,Country_Name);

        lstview=(ListView)findViewById(R.id.lst);
        // Assign adapter to ListView
        lstview.setAdapter(array);

        // ListView Item Click Listener
        lstview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Toast.makeText(MainActivity.this, Country_Name[position], Toast.LENGTH_LONG).show();
            }
        });
    }
}
