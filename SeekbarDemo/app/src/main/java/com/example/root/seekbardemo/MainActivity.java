package com.example.root.seekbardemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //create objects of textview and seekbar
    TextView txt;
    SeekBar sb;
    int progressValue=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         //bind the textbox and seekbar id
        txt=(TextView)findViewById(R.id.textView);
        sb=(SeekBar)findViewById(R.id.seekBar);

        txt.setText("Covered: " + sb.getProgress() + "/" + sb.getMax());

        //seekbar listener
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue=progress;
                txt.setText("Changed: " + progressValue + "/" + seekBar.getMax());
                txt.setTextSize(progressValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this,"Click",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                txt.setText("Covered: " + progressValue + "/" + seekBar.getMax());
            }
        });
    }



}
