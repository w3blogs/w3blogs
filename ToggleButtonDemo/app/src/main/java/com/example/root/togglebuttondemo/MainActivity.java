    package com.example.root.togglebuttondemo;

    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.widget.CompoundButton;
    import android.widget.Toast;
    import android.widget.ToggleButton;

    public class MainActivity extends AppCompatActivity {
    ToggleButton togle;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        togle =(ToggleButton) findViewById(R.id.toggleButton);

        togle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this, "Enabled", Toast.LENGTH_SHORT).show();
                } else {
                    // The toggle is disabled
                    Toast.makeText(MainActivity.this, "Disabled", Toast.LENGTH_SHORT).show();
                }

            }
        });

        }

    }
