package demo.com.splashscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Creting thrade
        Thread th = new Thread(){

            @Override
            public void run() {
                try{
                    // Declare how much slpash screen will display
                    sleep(2000);
                    Intent ob = new Intent(MainActivity.this,Main2Activity.class);
                    startActivity(ob);
                    finish();

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        };
        th.start();


    }

}