package demo.com.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    // Crate the object
    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Bind with id
        b1 = findViewById(R.id.btn);


        // Click Method Of The Button
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Creating Intent
                Intent ob = new Intent(getApplicationContext(),Main2Activity.class);
                startActivity(ob);

            }
        });

    }
}
