package com.example.recyclerdemo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Jay Mataji on 25-Sep-17.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyHolder> {

    String s1[], s2[];
    int s3[];
    Context ctx;

    public MyRecyclerAdapter(MainActivity ct, String[] d1, String[] d2, int[] d3) {
        ctx = ct;
        s1 = d1;
        s2 = d2;
        s3 = d3;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(ctx);
        View myView = layoutInflater.inflate(R.layout.list_row, parent, false);

        return new MyHolder(myView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {

        holder.img1.setImageResource(s3[position]);
        holder.txt1.setText(s1[position]);
        holder.txt2.setText(s2[position]);
    }

    @Override
    public int getItemCount() {
        return s1.length;
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        ImageView img1;
        TextView txt1, txt2;

        public MyHolder(View itemView) {
            super(itemView);

            img1 = (ImageView) itemView.findViewById(R.id.img1);
            txt1 = (TextView) itemView.findViewById(R.id.txt1);
            txt2 = (TextView) itemView.findViewById(R.id.txt2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent ob=new Intent(ctx,Main2Activity.class);
                    ob.putExtra("name",s1[getAdapterPosition()]);
                    ctx.startActivity(ob);
                }
            });
        }

    }
}
