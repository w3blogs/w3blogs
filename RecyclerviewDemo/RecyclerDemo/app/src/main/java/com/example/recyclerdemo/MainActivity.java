package com.example.recyclerdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;
    MyRecyclerAdapter myRecyclerAdapter;

    String data1[],data2[];
    int myImage[]={R.drawable.icon,R.drawable.icon,R.drawable.icon,R.drawable.icon,R.drawable.icon,R.drawable.icon,R.drawable.icon,R.drawable.icon,R.drawable.icon,R.drawable.icon};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data1=getResources().getStringArray(R.array.animal);
        data2=getResources().getStringArray(R.array.animal_desc);

        rv= (RecyclerView) findViewById(R.id.rv);

        myRecyclerAdapter=new MyRecyclerAdapter(this,data1,data2,myImage);

        rv.setAdapter(myRecyclerAdapter);
        rv.setLayoutManager(new LinearLayoutManager(this));

    }
}
