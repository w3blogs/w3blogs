package demo.com.radiobutton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Object Of The Button And Radio-Button
    Button btn;
    RadioButton rb1;
    RadioButton rb2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         // Binding With The Id
        btn = (Button) findViewById(R.id.btn007);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Check With Radio-Button Is Checked
                if (rb1.isChecked()) {
                    Toast.makeText(MainActivity.this, "Male Is Checked...!" , Toast.LENGTH_LONG).show();
                }
                if (rb2.isChecked()) {
                    Toast.makeText(MainActivity.this, "FeMale Is Checked...!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
