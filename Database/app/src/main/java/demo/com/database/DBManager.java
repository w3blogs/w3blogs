package demo.com.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

    /**
 * Created by Vivek on 4/14/2017.
 */

public class DBManager  {

    private DBHelper dbHelper;
    private Context context;
    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String name, String post, int salary, int ta,int da,int hra,int total) {

        ContentValues contentValue = new ContentValues();

        contentValue.put(DBHelper.EMP_SUBJECT, name);
        contentValue.put(DBHelper.EMP_post, post);
        contentValue.put(DBHelper.EMP_salary,salary);
        contentValue.put(DBHelper.EMP_ta,ta);
        contentValue.put(DBHelper.EMP_da,da);
        contentValue.put(DBHelper.EMP_hra,hra);
        contentValue.put(DBHelper.EMP_total,total);

        database.insert(DBHelper.TABLE_NAME, null, contentValue);
    }

    public Cursor fetch() {
        String[] columns = new String[] { DBHelper._ID, DBHelper.EMP_SUBJECT, DBHelper.EMP_post,DBHelper.EMP_salary,DBHelper.EMP_ta,DBHelper.EMP_da,DBHelper.EMP_hra,DBHelper.EMP_total};

        Cursor cursor = database.query(DBHelper.TABLE_NAME, columns, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int update(long _id, String name, String post,int salary,int ta,int da,int hra,int total) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(DBHelper.EMP_SUBJECT, name);
        contentValues.put(DBHelper.EMP_post, post);
        contentValues.put(DBHelper.EMP_salary,salary);
        contentValues.put(DBHelper.EMP_ta,ta);
        contentValues.put(DBHelper.EMP_da,da);
        contentValues.put(DBHelper.EMP_hra,hra);
        contentValues.put(DBHelper.EMP_total,total);

        int i = database.update(DBHelper.TABLE_NAME, contentValues, DBHelper._ID + " = " + _id, null);

        return i;
    }

    public void delete(long _id) {
        database.delete(DBHelper.TABLE_NAME, DBHelper._ID + "=" + _id, null);
    }

}

