package demo.com.database;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText txtId,txtName,txtpost,s,t,d,h,n;
    Button btnAdd,btnUpdate,btnDelete,btnFetch,b;
    DBManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dm=new DBManager(MainActivity.this);

        txtId= (EditText) findViewById(R.id.txtId);
        txtName= (EditText) findViewById(R.id.txtname);
        txtpost= (EditText) findViewById(R.id.txtpost);
        s = (EditText) findViewById(R.id.salary);
        t = (EditText) findViewById(R.id.ta);
        d = (EditText) findViewById(R.id.da);
        h = (EditText) findViewById(R.id.hra);
        n = (EditText) findViewById(R.id.total);
        btnAdd= (Button) findViewById(R.id.btnAdd);
        btnUpdate= (Button) findViewById(R.id.btnUpdate);
        btnDelete= (Button) findViewById(R.id.btnDelete);
        btnFetch= (Button) findViewById(R.id.btnFetch);
        b= (Button) findViewById(R.id.btn);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dm.open();

                String nm=txtName.getText().toString();
                String ct=txtpost.getText().toString();
                int sal = Integer.parseInt(s.getText().toString());
                int taa = Integer.parseInt(t.getText().toString());
                int daa = Integer.parseInt(d.getText().toString());
                int hraa = Integer.parseInt(h.getText().toString());
                int totall = Integer.parseInt(n.getText().toString());

                Toast.makeText(MainActivity.this, nm+ct+sal+taa+daa+hraa+totall, Toast.LENGTH_SHORT).show();

                dm.insert(nm,ct,sal,taa,daa,hraa,totall);
                dm.close();

                Toast.makeText(MainActivity.this, "Record Inserted", Toast.LENGTH_SHORT).show();
            }
        });


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dm.open();

                int  id=Integer.parseInt(txtId.getText().toString());
                String nm=txtName.getText().toString();
                String ct=txtpost.getText().toString();
                int sal = Integer.parseInt(s.getText().toString());
                int taa = Integer.parseInt(t.getText().toString());
                int daa = Integer.parseInt(d.getText().toString());
                int hraa = Integer.parseInt(h.getText().toString());
                int totall = Integer.parseInt(n.getText().toString());



                dm.update(id,nm,ct,sal,taa,daa,hraa,totall);

                dm.close();

                Toast.makeText(MainActivity.this, "Record Updated", Toast.LENGTH_SHORT).show();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dm.open();

                int  id=Integer.parseInt(txtId.getText().toString());
                dm.delete(id);

                dm.close();

                Toast.makeText(MainActivity.this, "Record Deleted", Toast.LENGTH_SHORT).show();
            }
        });

        btnFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dm.open();

                Cursor c=dm.fetch();
//                c.moveToNext();

                txtId.setText(c.getString(0));
                txtName.setText(c.getString(1));
                txtpost.setText(c.getString(2));
                s.setText(c.getString(3));
                t.setText(c.getString(4));
                d.setText(c.getString(5));
                h.setText(c.getString(6));
                n.setText(c.getString(7));
                dm.close();

                Toast.makeText(MainActivity.this, "Record Fetched", Toast.LENGTH_SHORT).show();

                c.moveToNext();
                Toast.makeText(MainActivity.this, c.getString(1), Toast.LENGTH_SHORT).show();
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int sa,da,oa,hra,total;

                sa = Integer.parseInt(s.getText().toString());

                da = sa *2/100;
                oa = sa*3/100;
                hra = sa*4/100;
                total = sa + da +oa +hra;


                s.setText(sa+"");
                t.setText(da+"");
                d.setText(oa+"");
                h.setText(hra+"");
                n.setText(total+"");

            }
        });
    }
}
