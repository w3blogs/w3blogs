package demo.com.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by Vivek on 4/14/2017.
 */

public class DBHelper extends SQLiteOpenHelper {


    // Database Information
    static final String DB_NAME = "EMPLOYEE.DB";

    // Table Name
    public static final String TABLE_NAME = "EMP";

    // Table columns
    public static final String _ID = "_id";
    public static final String EMP_SUBJECT = "name";
    public static final String EMP_post = "post";
    public static final String EMP_salary="salary";
    public static final String EMP_ta="ta";
    public static final String EMP_da="da";
    public static final String EMP_hra="hra";
    public static final String EMP_total="total_salary";

    // database version
    static final int DB_VERSION = 1;

    // Creating table query
    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EMP_SUBJECT + " TEXT NOT NULL, " + EMP_post + " TEXT," + EMP_salary + " INTEGER," +EMP_ta +" INTEGER," + EMP_da +" INTEGER,"+ EMP_hra +" INTEGER," +EMP_total + " INTEGER );";


    public DBHelper(Context ct) {
        super(ct, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

}