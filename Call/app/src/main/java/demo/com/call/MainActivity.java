package demo.com.call;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button b;
    EditText e;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = (Button) findViewById(R.id.btn1);
        e = (EditText) findViewById(R.id.e1);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String n = e.getText().toString();

                Intent call = new Intent(Intent.ACTION_CALL);
                call.setData(Uri.parse("tel:" + n));
                startActivity(call);
            }
        });

    }
}
