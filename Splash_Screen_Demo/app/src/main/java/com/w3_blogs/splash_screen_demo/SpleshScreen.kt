package com.w3_blogs.splash_screen_demo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler

/**
 * Created by jaimin_patel on 10/6/2017.
 */

class SpleshScreen : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splesh_screen)
        Handler().postDelayed({
            val i = Intent(this@SpleshScreen, MainActivity::class.java)
            startActivity(i)
            finish()
        }, SPLASH_TIME_OUT.toLong())

    }

    companion object {
        private val SPLASH_TIME_OUT = 1000
    }
}
