package demo.com.databasesimplecursoradapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ModifyTodoActivity extends Activity {
	
	private EditText titleText;
	private Button updateBtn, deleteBtn;
	private EditText descText;
	
	private long _id;
	
	private DBManager dbManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle("Modify Record");
		  
		setContentView(R.layout.activity_modify_record);

		dbManager = new DBManager(this);
		dbManager.open();

		titleText = (EditText) findViewById(R.id.subject_edittext);
		descText = (EditText) findViewById(R.id.description_edittext);
		
		updateBtn = (Button) findViewById(R.id.btn_update);
		deleteBtn = (Button) findViewById(R.id.btn_delete);

		Intent intent = getIntent();

		String id = intent.getStringExtra("id");
		String name = intent.getStringExtra("title");
		String desc = intent.getStringExtra("desc");

		_id = Long.parseLong(id);
		titleText.setText(name);
		descText.setText(desc);

		updateBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String title = titleText.getText().toString();
				String desc = descText.getText().toString();

				dbManager.update(_id, title, desc);
				returnHome();

			}
		});

		deleteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dbManager.delete(_id);
				returnHome();
			}
		});
	}

	public void returnHome() {

		Intent home_intent = new Intent(ModifyTodoActivity.this, TodoListActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		startActivity(home_intent);
	}
}