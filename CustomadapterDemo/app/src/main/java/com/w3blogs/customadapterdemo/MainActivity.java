package com.w3blogs.customadapterdemo;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.w3blogs.customadapterdemo.Adapter.CustomAdapter;
import com.w3blogs.customadapterdemo.Model.DataModel;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    ArrayList<DataModel> data;
    ListView lst;
    private static CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lst = (ListView) findViewById(R.id.customlist);

        data = new ArrayList<>();
        data.add(new DataModel(R.drawable.cat,"Bear"));
        data.add(new DataModel(R.drawable.panda, "Cat"));
        data.add(new DataModel(R.drawable.tiger, "Deer"));
        data.add(new DataModel(R.drawable.panda, "Dog"));
        data.add(new DataModel(R.drawable.cat, "Elephant"));
        data.add(new DataModel(R.drawable.tiger, "Lion"));
        data.add(new DataModel(R.drawable.panda, "Monkey"));
        data.add(new DataModel(R.drawable.cat, "Panda"));
        data.add(new DataModel(R.drawable.tiger, "Tiger"));

        adapter = new CustomAdapter(data, getApplicationContext());
        lst.setAdapter((ListAdapter) adapter);

    }
}
