package com.w3blogs.customadapterdemo.Adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.w3blogs.customadapterdemo.Model.DataModel;
import com.w3blogs.customadapterdemo.R;

import java.util.ArrayList;
import java.util.Locale;

public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener{

    private ArrayList<DataModel> dataSet;
    private ArrayList<DataModel> arraylist;
    Context mContext;
    LayoutInflater inflater;


    // View lookup cache
    private static class ViewHolder {
        ImageView imgdemo;
        TextView txtName;
    }


    public CustomAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.activity_design, data);
        this.dataSet = data;
        this.mContext=context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<DataModel>();
        this.arraylist.addAll(data);

    }

    @Override
    public void onClick(View view) {

        int position=(Integer) view.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;

    }
    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataModel dataModel = getItem(position);
        final ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.activity_design, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.textView);
            viewHolder.imgdemo = (ImageView) convertView.findViewById(R.id.imageView);
            result=convertView;

                convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
            viewHolder.txtName.setText(dataSet.get(position).getNameofprogram());
            viewHolder.imgdemo.setImageResource(dataSet.get(position).getImage());
            return convertView;


        }



        viewHolder.txtName.setText(dataModel.getNameofprogram());
        viewHolder.imgdemo.setImageResource(dataModel.getImage());
        viewHolder.imgdemo.setTag(dataModel.getImage());
        viewHolder.imgdemo.setOnClickListener(this);
        return convertView;
    }
}