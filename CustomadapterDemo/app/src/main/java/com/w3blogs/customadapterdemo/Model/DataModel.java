package com.w3blogs.customadapterdemo.Model;

/**
 * Created by root on 11/12/17.
 */

public class DataModel {
    private int image;
    private String nameofprogram;


    public DataModel(int image, String nameofprogram) {
        this.image = image;
        this.nameofprogram = nameofprogram;

    }


    public String getNameofprogram() {
        return nameofprogram;
    }

    public void setNameofprogram(String nameofprogram) {
        this.nameofprogram = nameofprogram;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
